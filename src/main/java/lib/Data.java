package lib;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Data {

	private static Map<Long, HashMap<String, String>> globalTestdata = 
			               new HashMap<Long, HashMap<String, String>>();
	
	private static Map<Long, List<LinkedHashMap<String, String>>> globalTestdataExtItr = 
            new HashMap<Long, List<LinkedHashMap<String, String>>>();
	
	private static LinkedHashMap<Integer, LinkedHashMap<String, String>> allItrTestdata = 
            			   new LinkedHashMap<Integer, LinkedHashMap<String, String>>();
	
	private static LinkedHashMap<Integer, LinkedHashMap<String, String>> allExtrnalData = 
			new LinkedHashMap<Integer, LinkedHashMap<String, String>>(); 
	
	/**
	 * <h1>setVal</h1> loads test data and tags every data set extracted from data sheet to the iteration number 
	 * 
	 * @param HashMap<String, String> td
	 * @return void
	 * @author duttasoc
	 * @date 10/12/2016
	 */
	@SuppressWarnings("unused")
	private static void setVal(HashMap<String, String> td){
		globalTestdata.put(Thread.currentThread().getId(), td);
	}
	
	/**
	 * <h1>setTestDataForSingleItr</h1> loads test data and tags all data set extracted from data sheet to a single iteration number
	 * 
	 * @param HashMap<String, String> td
	 * @return void
	 * @author duttasoc
	 * @date 25/08/2017
	 */
	@SuppressWarnings("unused")
	private static void setTestDataForSingleItr(List<LinkedHashMap<String, String>> td){
		globalTestdataExtItr.put(Thread.currentThread().getId(), td);
	}
	
	/**
	 * <h1>setCompleteData</h1> loads complete data set to access any iteration from current iteration
	 * 
	 * @param LinkedHashMap<Integer, LinkedHashMap<String, String>> td
	 * @return void
	 * @author duttasoc
	 * @date 10/08/2017
	 */
	@SuppressWarnings("unused")
	private static void setCompleteData(LinkedHashMap<Integer, LinkedHashMap<String, String>> td){
		allItrTestdata = td;	
	}
	
	/**
	 * <h1>setValExtData</h1> loads complete external data set from external data file
	 * 
	 * @param LinkedHashMap<Integer, LinkedHashMap<String, String>> td
	 * @return void
	 * @author duttasoc
	 * @date 15/08/2017
	 */
	@SuppressWarnings("unused")
	private static void setValExtData(LinkedHashMap<Integer, LinkedHashMap<String, String>>  extdata){
		allExtrnalData = extdata;
	}
	
	/**
	 * <h1>getCurrItrDataMap</h1> gets current iteration data to report them as String
	 * 
	 * @return List<LinkedHashMap<String, String>> 
	 * @author duttasoc
	 * @date 05/07/2017
	 */
	@SuppressWarnings("unused")
	private static Map<String,String> getCurrItrDataMap(){
		if(!globalTestdata.isEmpty()){
			return globalTestdata.get(Thread.currentThread().getId());
		}
		return null;
	}
	
	/**
	 * <h1>getVal</h1> gets testdata parameter value
	 * 
	 * @param String paramName
	 * @return String
	 * @author duttasoc
	 * @date 15/12/2016
	 */
	public static String getVal(String paramName){
		String value = "";
		try{
			if (globalTestdata.get(Thread.currentThread().getId()).containsKey(paramName.toUpperCase().trim())) {
				if (globalTestdata.get(Thread.currentThread().getId()).get(paramName.trim().toUpperCase()).length() > 0)
					value = globalTestdata.get(Thread.currentThread().getId()).get(paramName.trim().toUpperCase());
			} 
		}catch(Exception e){
			// do nothing
		}
		return value.trim();
	}
	
	/**
	 * <h1>getTestDataForSingleItr</h1> gets testdata map list tag to single itr
	 * 
	 * @return List<LinkedHashMap<String, String>> 
	 * @author duttasoc
	 * @date 05/07/2017
	 */
	public static List<LinkedHashMap<String, String>> getTestDataForSingleItr(){
		try{
			if (globalTestdataExtItr.get(Thread.currentThread().getId())!=null) {
				return globalTestdataExtItr.get(Thread.currentThread().getId());
			} 
		}catch(Exception e){
			// do nothing
		}
		return null;
	}

	/**
	 * <h1>getCompleteDataMap</h1> gets access to complete data set to access any iteration from current iteration
	 * 
	 * @return LinkedHashMap<Integer, LinkedHashMap<String, String>>
	 * @author duttasoc
	 * @date 10/05/2017
	 */
	public static LinkedHashMap<Integer, LinkedHashMap<String, String>> getCompleteDataMap() {
		if(!allItrTestdata.isEmpty()){
			return allItrTestdata;
		}
		return null;
	}
	

	/**
	 * <h1>getExternalData</h1> gets access to complete external data set from external data file
	 * 
	 * @param int Row
	 * @param String setColumnHeaderRow
	 * @return String
	 * @author duttasoc
	 * @date 15/12/2016
	 */
	public static String getExternalData(int Row, String setColumnHeaderRow){
		String val = "";
		try{
			if(allExtrnalData != null | !allExtrnalData.isEmpty()){
			   val = allExtrnalData.get(Row).get(setColumnHeaderRow.toUpperCase());
			}
		}catch(Exception e){
			// do nothing
		}
		return val.trim();
	}
	
	
	
}
