package core.framework.Web.Poc;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import lib.Common;
import util.Log;
import util.Log.Priority;

public class BrowserFactory {
	
	private static WebDriver driver = null;
	private static Map<Long,WebDriver> driverThread = new HashMap<Long,WebDriver>();
	private static Log log = new Log(BrowserFactory.class);

	
		
	public static WebDriver getDriverThread(long threadID) {
		return driverThread.get(threadID);
	}

	private static void setDriverThread(long threadID,WebDriver driver) {
				driverThread.put(threadID, driver);
	}

	protected static synchronized void setWebDriver(String webBrowser) {
		try{
			if(driverThread.get(Thread.currentThread().getId()) == null){
				
				if (webBrowser.trim().equalsIgnoreCase("INTERNET_EXPLORER") ||
				    webBrowser.trim().equalsIgnoreCase("IEXPLORE") ||
				    webBrowser.trim().equalsIgnoreCase("IE")) {
					/*DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
					capabilities.setCapability("ignoreZoomSetting", true);
					capabilities.setCapability("ie.ensureCleanSession", true);
					System.setProperty("webdriver.ie.driver",Common.getGlobalParam("IEDriverClassPath"));
					driver = new InternetExplorerDriver(capabilities);
					 */
					
				} else if (webBrowser.trim().equalsIgnoreCase("CHROME")) {			
					File file = new File(Globals.GC_DEFAULT_FILE_DOWLOAD_LOC);
					if(!file.exists()){file.mkdirs();}
					String downloadFilepath = Globals.GC_DEFAULT_FILE_DOWLOAD_LOC;
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("profile.default_content_settings.popups", 0);
					chromePrefs.put("download.default_directory", downloadFilepath);
					
					System.setProperty("webdriver.chrome.driver",Common.getGlobalParam("ChromeDriverPath"));
					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", chromePrefs);
					options.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation")); 
                    options.addArguments("test-type");
                    options.addArguments("start-maximized");
                    options.addArguments("disable-infobars");
					DesiredCapabilities cap = DesiredCapabilities.chrome();
					cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					cap.setCapability(ChromeOptions.CAPABILITY, options);
					options.addArguments("--start-maximized");
					
					driver = new ChromeDriver(cap);     
				} else if (webBrowser.trim().equalsIgnoreCase("FIREFOX") ||
						   webBrowser.trim().equalsIgnoreCase("FF")) {				
					File pathToBinary = new File(Common.getGlobalParam("FirefoxBinaryPath"));
					FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
					ProfilesIni profiles = new ProfilesIni();
					FirefoxProfile ffProfile = profiles.getProfile("default");
					// ffProfile.setPreference("signon.autologin.proxy", true);
					
					if (ffProfile == null) {
						System.out.println("Initiating Firefox with dynamic profile");									
						driver = new FirefoxDriver();
					} else {
						System.out.println("Initiating Firefox with default profile");										
						driver = new FirefoxDriver(ffBinary,ffProfile);
					}
				} else {
					throw new Error("Unknown browser type specified: " + webBrowser);
				}	
				
				// setting event firing webdriver
				EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
				TestListener listener = new TestListener();
				eventDriver.register(listener);
				
				// Implicit timeout
				/*driver.manage().timeouts().implicitlyWait(Long.parseLong(Common.getGlobalParam(""
													  + "GLB_OBJ_SYNC_TIMEOUT")),TimeUnit.SECONDS);*/	
				// PageLoad timeout
				driver.manage().timeouts().pageLoadTimeout(Long.parseLong(Common.getGlobalParam(""
													  + "PG_LOAD_TIMEOUT")), TimeUnit.SECONDS);
							
				Capabilities cap = ((RemoteWebDriver)driver).getCapabilities();		
		        String browserName = cap.getBrowserName().toUpperCase();
		        Common.setGlobalParam("BROWSER_NAME", browserName);	
		        System.out.println("BROWSER NAME:"+browserName);
		        String os = cap.getPlatform().toString();
		        System.out.println("OPERATING SYSTEM:"+os);
		        String browserVersion = cap.getVersion().toString().substring(0, 4);
		        Common.setGlobalParam("BROWSER_VER", browserVersion);	
		        System.out.println("BROWSER VERSION:"+browserVersion);	       
		        
		        setDriverThread(Thread.currentThread().getId(),eventDriver);
			}
		}catch(Exception e){
			log.Report(Priority.ERROR, "Unable to initialize web driver :"+e.getMessage());		
		}
	}
	
}
