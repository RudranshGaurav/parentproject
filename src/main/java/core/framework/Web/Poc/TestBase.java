package core.framework.Web.Poc;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.annotations.DataProvider;
import lib.Common;
import lib.Data;
import util.Log;
import util.Log.Priority;
import util.reporter.IReporter;
import util.reporter.PDFReporter;
import util.reporter.ReportManager;
import util.reporter.Status;

public class TestBase {
	protected static String MANUAL_TC_NAME = Globals.GC_EMPTY; 
	protected static LinkedHashMap<Integer, LinkedHashMap<String, String>> testDataObj = null;
	protected static LinkedHashMap<Integer, List<LinkedHashMap<String, String>>> singleItrAllTetsDataObj = null;
	private static Map<Long,String> manualTCName = new HashMap<Long,String>();
	private LinkedHashMap<Integer, LinkedHashMap<String, String>> extrnlData = null;
	private Log log = new Log(TestBase.class);
	
	/**
	 * <h1>reporter</h1> used for logging HTML report
	 * 
	 * @return void
	 * @author duttasoc
	 * @date 11/01/2017
	 */
	protected IReporter reporter() {
		return ReportManager.GetReporter();
	}
	
	/**
	 * <h1>setOQDataMap</h1> used to set OQ custom data for PDF Report generation
	 * 
	 * @param String key
	 * @param String val
	 * @return void
	 * @author duttasoc
	 * @date 11/01/2017
	 */
	protected void setOQDataMap(String Key,String Val) {
		if(((PDFReporter) ReportManager.GetPDF()) != null) {
			((PDFReporter) ReportManager.GetPDF()).UpdateOQData(Key, Val);
		}
	}

	/**
	 * <h1>pdfReporter</h1> used for logging only pdf report as per Novartis SAP Hybris OQ format
	 * 
	 * @return PDFReporter
	 * @author duttasoc
	 * @date 15/01/2017
	 */
	protected PDFReporter pdfReporter() {
		return ((PDFReporter) ReportManager.GetPDF());
	}
	
	/**
	 * <h1>driver</h1> provides access to WebDriver object
	 * 
	 * @return WebDriver
	 * @author duttasoc
	 * @date 12/12/2016
	 */
	protected WebDriver driver(){
		return BrowserFactory.getDriverThread(Thread.currentThread().getId());
	}
	
	/**
	 * <h1>getManualTCName</h1> returns manual test case name to report, prefered to use for an automation test case
	 * pointing to multiple manual test case names
	 * 
	 * @return String
	 * @author duttasoc
	 * @date 05/04/2017
	 */
	protected String getManualTCName(){
		return manualTCName.get(Thread.currentThread().getId());
	}
	
	/**
	 * <h1>importExternalData</h1> imports external test data supplied through 
	 * external data sheet other than usual test data sheet 
	 * 
	 * @param String xlFilePath
	 * @param int sheetIndex
	 * @param int setColumnHeaderRow
	 * @return void
	 * @author duttasoc
	 * @date 05/07/2017
	 */
	public void importExternalData(String xlFilePath,int sheetIndex,int setColumnHeaderRow){
		try{
			GetXLData xl = new GetXLData();
			extrnlData = xl.getExternalTestDataMap(xlFilePath, sheetIndex, setColumnHeaderRow);
			
			Method m = Data.class.getDeclaredMethod("setValExtData", LinkedHashMap.class);
			m.setAccessible(true);
			m.invoke(null, extrnlData);
		}catch(Exception e){
			log.Report(Priority.ERROR, "Unable to import external data :"+e.getMessage());
		}
	}
	
	/**
	 * <h1>getExtrnlDataSize</h1> returns max data row of the external data
	 * 
	 * @return int
	 * @author duttasoc
	 * @date 05/07/2017
	 */
	public int getExtrnlDataSize(){
		if(!extrnlData.isEmpty()){
		   return extrnlData.size();
		}
		return 0;
	}
	
	/**
	 * <h1>setData</h1> its a data provider method used for 
	 * every data set extracted from data sheet to the iteration number 
	 * 
	 * @param ITestNGMethod method
	 * @param ITestContext test
	 * @return Object[][]
	 * @author duttasoc
	 * @date 10/12/2016
	 */
	@DataProvider
	public Object[][] setData(ITestNGMethod method, ITestContext test){
		String tcClassPkgNm = Globals.GC_EMPTY;
		String tcName = Globals.GC_EMPTY;
		IGetData getData = null;
		Object[][] dataProviderObj = null;
	
		try{
			tcClassPkgNm = method.getRealClass().getPackage().getName();
			tcName = test.getName();
			manualTCName.put(Thread.currentThread().getId(), tcName);
			log.Report(Priority.INFO,"Setting data provider with TestData "
			                  +Common.getGlobalParam("TESTDATA_FORMAT"));
		    if(Common.getGlobalParam("TESTDATA_FORMAT").equals("XLSX")
		       ||Common.getGlobalParam("TESTDATA_FORMAT").equals("XLS")){
		       getData = new GetXLData();
		    }
		    if(Common.getGlobalParam("TESTDATA_FORMAT").equals("XML")){
		       getData = new GetXMLData();
		    }
		    testDataObj = getData.getTestDataMap(tcClassPkgNm, tcName);
		    if (testDataObj != null) {
		    	dataProviderObj = new Object[testDataObj.size()][2];
				int count = 0;
				
				for(Integer itr : testDataObj.keySet()){
					dataProviderObj[count][0] = itr;
					dataProviderObj[count][1] = null;
					count++;
				}
				PDFReporter.SetDataItrCount(testDataObj.size());
				
				return dataProviderObj;
			} 
		}catch(Exception e){
			log.Report(Priority.ERROR, "Unable to initialize DataProvider :"+e.getMessage());
		}
		return new Object[][] {{}}; // As null cannot be returned in DataProvider	
	}

	/**
	 * <h1>setAllDataAtSingleItr</h1> its a data provider method tags all data set extracted 
	 *  from data sheet to a single iteration number 
	 * 
	 * @param ITestNGMethod method
	 * @param ITestContext test
	 * @return Object[][]
	 * @author duttasoc
	 * @date 05/07/2017
	 */
	@DataProvider
	public Object[][] setAllDataAtSingleItr(ITestNGMethod method, ITestContext test){
		String tcClassPkgNm = Globals.GC_EMPTY;
		String tcName = Globals.GC_EMPTY;
		IGetData getData = null;
		Object[][] dataProviderObj = null;
	
		try{
			tcClassPkgNm = method.getRealClass().getPackage().getName();
			tcName = test.getName();
			manualTCName.put(Thread.currentThread().getId(), tcName);
			log.Report(Priority.INFO,"Setting data provider with TestData "
			                  +Common.getGlobalParam("TESTDATA_FORMAT"));
		    if(Common.getGlobalParam("TESTDATA_FORMAT").equals("XLSX")
		       ||Common.getGlobalParam("TESTDATA_FORMAT").equals("XLS")){
		       getData = new GetXLData();
		    }
		    if(Common.getGlobalParam("TESTDATA_FORMAT").equals("XML")){
		       getData = new GetXMLData();
		    }
		    singleItrAllTetsDataObj = getData.getAllDataForSingleItr(tcClassPkgNm, tcName);
		    if (singleItrAllTetsDataObj != null) {
		    	dataProviderObj = new Object[singleItrAllTetsDataObj.size()][2];
				int count = 0;
				
				for(Integer itr : singleItrAllTetsDataObj.keySet()){
					dataProviderObj[count][0] = itr;
					dataProviderObj[count][1] = null;
					count++;
				}
				//PDFReporter.SetDataItrCount(singleItrAllTetsDataObj.size());
				
				return dataProviderObj;
			} 
		}catch(Exception e){
			log.Report(Priority.ERROR, "Unable to initialize DataProvider :"+e.getMessage());
		}
		return new Object[][] {{}}; // As null cannot be returned in DataProvider	
	}
	
	/**
	 * <h1>ReportIterationData</h1> converts every iteration data into a String, used for reporting data in HTML
	 * 
	 * @param int itr
	 * @return void
	 * @author duttasoc
	 * @date 05/02/2017
	 */
	@SuppressWarnings("unchecked")
	protected void ReportIterationData(int itr){
		try{
			Map<String,String> mp = null;
			//Setting all itr data - to access any itr data from current itr
			Method m1 = Data.class.getDeclaredMethod("getCurrItrDataMap");
			m1.setAccessible(true);
			mp = (Map<String,String>) m1.invoke(null);
			reporter().Log(Status.INFO,"Test data for iteration : "+ itr,
				       Common.getIterationDataAsString(mp),false);
		}catch(Exception e){
			log.Report(Priority.ERROR, "Failed to report iteration data :"+e.getMessage());
		}
	}

}
