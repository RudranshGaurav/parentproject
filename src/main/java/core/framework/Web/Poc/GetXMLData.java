package core.framework.Web.Poc;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import lib.Common;
import util.Log;
import util.Log.Priority;

public class GetXMLData implements IGetData{
	private Log log = new Log(GetXMLData.class);

	/*
	 * Generating test data map by reading data from xml
	*/
	@Override
	public LinkedHashMap<Integer, LinkedHashMap<String, String>> getTestDataMap(String tcClassPkgNm, String tcName) {
		log.Report(Priority.INFO, Globals.GC_LOG_INITTC_MSG + tcClassPkgNm + "."+ tcName + Globals.GC_LOG_INITTC_MSG);
		LinkedHashMap<Integer, LinkedHashMap<String, String>> td = null;
		LinkedHashMap<String, String> mapData = null;		
		String appName = Common.getGlobalParam("AUT");
		String modName = tcClassPkgNm.split("\\.")[(tcClassPkgNm.split("\\.").length) - 1];

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			File xmlfile = new File(Globals.GC_TESTDATALOC + Globals.GC_TESTDATAPREFIX + appName + "_"
					       + Common.getGlobalParam("TEST_ENV") + ".xml");
			
			// Setting up common test data location
			if (System.getProperties().containsKey("testDataPath")
					&& System.getProperty("testDataPath").equalsIgnoreCase("true")) {
				xmlfile = new File(Globals.GC_COMMON_TESTDATALOC + appName + "\\\\" + Globals.GC_TESTDATAPREFIX
						+ appName + "_" + Common.getGlobalParam("TEST_ENV") + ".xml");
			} else {
				xmlfile = new File(Globals.GC_TESTDATALOC + Globals.GC_TESTDATAPREFIX + appName + "_"
						+ Common.getGlobalParam("TEST_ENV") + ".xml");
			}

			
			Document doc = dBuilder.parse(xmlfile);
			doc.getDocumentElement().normalize();
			td = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();
			NodeList nList = doc.getElementsByTagName("Module");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String name = eElement.getAttribute("name");
					if (name.equals(modName)) {
				
						NodeList testcaseList = eElement.getElementsByTagName("TestCase");
						for (int temp1 = 0; temp1 < testcaseList.getLength(); temp1++) {
							Node testCaseNode = testcaseList.item(temp1);
							Element testCaseElement = (Element) testCaseNode;
							
							if (testCaseElement.getAttribute("name").equals(tcName)) {
								NodeList iterationList = testCaseElement.getElementsByTagName("Iteration"); //
								
								for (int iteration = 0; iteration < iterationList.getLength(); iteration++) {
									Node iterationNode = iterationList.item(iteration);
									Element iterationElement = (Element) iterationNode;

									if (iterationElement.getAttribute("Execution").equals("Yes")) {
										mapData = new LinkedHashMap<String, String>();
										int key = Integer.parseInt(iterationElement.getAttribute("id"));

										NodeList parameterList = iterationElement.getElementsByTagName("parameter");
										for (int parameter = 0; parameter < parameterList.getLength(); parameter++) {
											Node parameterNode = parameterList.item(parameter);
											Element parameterElement = (Element) parameterNode;
											
											mapData.put(parameterElement.getAttribute("name").toUpperCase(),
													parameterElement.getAttribute("value"));
											
											log.Report(Priority.DEBUG,"test data mapped for index "+ key
													   + " with key "+ parameterElement.getAttribute("name").toUpperCase()
													   + " and value "+ parameterElement.getAttribute("value"));
											
										}
										if (!td.containsKey(key))td.put(key, mapData);
									}
								}
								break;
							}
						}
						break;
					}
				}
			} 
			return td;
		} catch (Exception e) {
			log.Report(Priority.ERROR,  "Exception at getting XML Data : " + e.getMessage());
		}
		return null;
	}

	@Override
	public LinkedHashMap<Integer, List<LinkedHashMap<String, String>>> getAllDataForSingleItr(String tcClassPkgNm,
			String tcName) {
		// Not implemented
		return null;
	}
	
}
