package core.framework.Web.Poc;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import lib.Common;
import util.Log;
import util.Log.Priority;

public class RetryAnalyzer extends TestBase implements IRetryAnalyzer  {

	private int counter = 1;
	private int retryLimit = 0;
	//private String failed_tc_name = "";
	private Log log = new Log(RetryAnalyzer.class);
	
	/*public String getFailed_tc_name() {
		return failed_tc_name;
	}

	public void setFailed_tc_name(String failed_tc_name) {
		failed_tc_name = failed_tc_name;
	}*/

	@Override
	public boolean retry(ITestResult result) {
		retryLimit = Integer.parseInt(Common.getGlobalParam("FAILED_TC_RETRY_COUNT").trim());
		try {
			if (counter <= retryLimit) {
			//	setFailed_tc_name(result.getTestContext().getName() + "_Retry_" + counter );
				counter++;
				
				if (!driver().getWindowHandle().isEmpty()){
				   driver().quit();
				}
						
				// Setting driver to null so that it can reinitialize after quit
				Method m = BrowserFactory.class.getDeclaredMethod("setDriverThread", long.class, WebDriver.class);
				m.setAccessible(true);
				m.invoke(null, Thread.currentThread().getId(), null);

				BrowserFactory.setWebDriver(Common.getGlobalParam("BROWSER"));
				log.Report(Priority.INFO, "Web Driver instance re-initiated for failed test cases retry" + "Test Case : " + result.getTestContext().getName());
				return true;
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
		return false;
	}
	
	

}
