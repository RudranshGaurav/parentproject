package util;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

public class CSV_ReadWrite {
	
	private List<String[]> data = null;
	private CSVReader csv = null;
	
	public CSV_ReadWrite(String filePath) throws Exception{
		try{
			CSVReader csv = new CSVReader(new FileReader(filePath),',','"',0);
			data = new ArrayList<String[]>();
			data = csv.readAll();
			csv.close();
		}catch(Exception e){
			throw new Exception("Exception occurred while initalizing CSV .Exception details : "+ e.getMessage());
		}
	}
	
	public List<String[]> getCSVData(){
		if(!data.isEmpty()){
			return data;
		}
		return null;
	}
	
	
}
