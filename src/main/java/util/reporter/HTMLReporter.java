package util.reporter;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import core.framework.Web.Poc.Globals;
import core.framework.Web.Poc.TestBase;
import lib.Common;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import util.Log;
import util.Log.Priority;

public class HTMLReporter extends TestBase implements IReporter{

	ExtentReports reporter = null;
	Map<Long,ExtentReports> mapExtent = null;
	Map<Long, ExtentTest> loggers = null;
	File files = null;
	File videoFile = null;
	//private ScreenRecorder screenRecorder;
	private static Log log = new Log(HTMLReporter.class);

	
	public HTMLReporter(String filePath) {
		files = new File(Globals.GC_TEST_HTMLREPORT_DIR);
		if(!files.exists()){files.mkdirs();}
		reporter = new ExtentReports(Globals.GC_TEST_HTMLREPORT_DIR+"\\"+filePath, false);
		reporter.loadConfig(new File(Globals.GC_EXTENT_CONFIG_LOC));
		loggers = new HashMap<Long, ExtentTest>();
	}
	
	@Override
	public void StartTest(String testName) {
		long threadId = Thread.currentThread().getId();
		loggers.put(threadId, reporter.startTest(testName));
		/*if(!Common.getGlobalParam("CAPTURESCREENCAST").equals("NEVER")){
		   startRecording(testName);	   
		}*/	
		//loggers.put(threadId, mapExtent.get(threadId).startTest(testName));
	}

	@Override
	public void Log(Status logStatus, String stepName, String stepDetails, boolean captureScreen) {
		long threadId = Thread.currentThread().getId(); 
		
		switch(Common.getGlobalParam("CAPTURESCREENSHOT").toLowerCase().trim()){
			case "always" : captureScreen = true; break;
			case "never"  : captureScreen = false; break;
			case "user_defined" : break;
			case "on_faliure_only" : 
				  if(logStatus.equals(Status.FAIL)){captureScreen = true;} break;
			default : break;
		}
			
		// not to try capturing screenshot if driver is null
		if(driver() == null) captureScreen = false;
		
		// Adding to take full screen shot for an exception occurred only for Web AUTs
		/*SET_SCREEN_CAPTURE = Common.getGlobalParam("CAPTURE_SCREEN_TYPE");
		if(stepName.toLowerCase().contains("exception") && captureScreen){
			Common.setGlobalParam("CAPTURE_SCREEN_TYPE", "FULL_SCREEN");  
		}*/
		
		if(loggers.containsKey(threadId)) {
			if(!captureScreen){
			   switch(logStatus){
				   case PASS : loggers.get(threadId).log(LogStatus.PASS,stepName,stepDetails);break;
				   case FAIL : loggers.get(threadId).log(LogStatus.FAIL,stepName,stepDetails);break;
				   case WARN : loggers.get(threadId).log(LogStatus.WARNING,stepName,stepDetails);break;
				   case INFO : loggers.get(threadId).log(LogStatus.INFO,stepName,stepDetails);break;
				   default: loggers.get(threadId).log(LogStatus.UNKNOWN,"Invalid step","Invalid step details");
			   }
			}else{
				switch(logStatus){
				   case PASS : loggers.get(threadId).log(LogStatus.PASS,stepName,stepDetails 
						     + loggers.get(threadId).addScreenCapture(captureScreen()));break;
				   case FAIL : loggers.get(threadId).log(LogStatus.FAIL,stepName,stepDetails 
						     + loggers.get(threadId).addScreenCapture(captureScreen()));break;
				   case WARN : loggers.get(threadId).log(LogStatus.WARNING,stepName,stepDetails 
						     + loggers.get(threadId).addScreenCapture(captureScreen()));break;
				   case INFO : loggers.get(threadId).log(LogStatus.INFO,stepName,stepDetails);break;
				   default: loggers.get(threadId).log(LogStatus.UNKNOWN,"Invalid step","Invalid step details");
			   }
			}
		}	
		
		// Resetting screen capture policy for Exception
		/*if(stepName.toLowerCase().contains("exception")){
		   Common.setGlobalParam("CAPTURE_SCREEN_TYPE", SET_SCREEN_CAPTURE); 
		   // scrolling page to the top
		   ((JavascriptExecutor) driver()).executeScript("window.scrollTo(0,0)");
		}*/
	}
	
	@Override
	public void EndTest(ITestResult result) {
		long threadId = Thread.currentThread().getId();	
		if(loggers.containsKey(threadId))
		{
			String recordLink ="";
			/*if(!Common.getGlobalParam("CAPTURESCREENCAST").equals("NEVER")){
			   recordLink = stopRecording(result.getTestContext().getName());
			}*/	
			
			if(loggers.get(threadId).getRunStatus().equals(LogStatus.FAIL)){
			   /*loggers.get(threadId).log(LogStatus.INFO,"Screen record",
					   "<a href="+recordLink+" src="+recordLink+">PLAY TEST RUN SCREENCAST</a>");*/
			   result.setStatus(ITestResult.FAILURE);
			}
			if(loggers.get(threadId).getRunStatus().equals(LogStatus.PASS)){
				/*if(Common.getGlobalParam("CAPTURESCREENCAST").equals("ON_FAILURE_ONLY")){	
					try {		
						Path path = Paths.get(Globals.GC_TEST_HTMLREPORT_DIR+recordLink);
						Files.deleteIfExists(path);
					} catch (IOException e) {			
						e.printStackTrace();
					}  
				}else if (Common.getGlobalParam("CAPTURESCREENCAST").equals("ALWAYS")){		 
					  loggers.get(threadId).log(LogStatus.INFO,"Screen record",
							  "<a href="+recordLink+" src="+recordLink+">PLAY TEST RUN SCREENCAST</a>");
				}*/
			    result.setStatus(ITestResult.SUCCESS);
			}
			if(loggers.get(threadId).getRunStatus().equals(LogStatus.WARNING)){
			   result.setStatus(ITestResult.SKIP);
			}		
			reporter.endTest(loggers.get(threadId));
			System.out.println(result.getName());
		}
	}

	@Override
	public void FlushReport() {
		reporter.flush();
	}
	
	@Override
	public void CloseReport() {
		reporter.close();
	}
	
	private synchronized String captureScreen(){
		if(Common.getGlobalParam("CAPTURE_SCREEN_TYPE").equalsIgnoreCase("FULL_SCREEN")){
			return captureFullScreen();
		} else { // by default - focused screen capture
			return captureFocusedScreen();
		}
	}
	
	private synchronized String captureFocusedScreen(){
		try{
			String timeStamp =  Globals.GC_EMPTY;
			if(driver()!=null){
				files = new File(Globals.GC_TEST_HTMLREPORT_DIR+"\\Screens");
				if(!files.exists()){files.mkdirs();}
				try {
				Thread.sleep(500);
				DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HHmmss");
				Date date = new Date();
				timeStamp = dateFormat.format(date);
				
					TakesScreenshot screen = (TakesScreenshot)driver();
					File fileSrc = screen.getScreenshotAs(OutputType.FILE);
					String rel = "\\Screens\\"+timeStamp+".png";
					String dest = Globals.GC_TEST_HTMLREPORT_DIR+rel;
					
					File fileDestn = new File(dest);
					FileUtils.copyFile(fileSrc, fileDestn);
					
					return "."+rel;
				} catch (IOException | InterruptedException e) {
					log.Report(Priority.ERROR,"Exception : "+e.getMessage());	
				}
			}
			
		}catch(Exception e){
			log.Report(Priority.ERROR, Common.shortenedStackTrace(e,10));	
		}
		return "";
		
	}
    
	private synchronized String captureFullScreen(){
		String timeStamp = Globals.GC_EMPTY;
		try {
			if (driver() != null) {
				files = new File(Globals.GC_TEST_HTMLREPORT_DIR + "\\Screens");
				if (!files.exists()) {
					files.mkdirs();
				}

				Thread.sleep(500);
				DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HHmmss");
				Date date = new Date();
				timeStamp = dateFormat.format(date);
				String rel = "\\Screens\\" + timeStamp + ".png";
				String dest = Globals.GC_TEST_HTMLREPORT_DIR + rel;

				Screenshot ss = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100))
						.takeScreenshot(driver());
				ImageIO.write(ss.getImage(), "PNG", new File(dest));
				return "." + rel;

			}
		} catch (IOException | InterruptedException e) {
			log.Report(Priority.ERROR, "Exception : " + e.getMessage());
		}
		return "";
	}
	
	/*public void startRecording(String tcName) {
		try{
			String dest = Globals.GC_TEST_HTMLREPORT_DIR+"\\ScreenRecord\\";
			
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			int width = screenSize.width;
			int height = screenSize.height;

			Rectangle captureSize = new Rectangle(0, 0, width, height);

			GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
					.getDefaultConfiguration();

			this.screenRecorder = new SpecializedScreenRecorder(gc, captureSize,
					new Format(FormatKeys.MediaTypeKey, MediaType.FILE, FormatKeys.MimeTypeKey, FormatKeys.MIME_AVI),
					new Format(FormatKeys.MediaTypeKey, MediaType.VIDEO, FormatKeys.EncodingKey,
							VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, VideoFormatKeys.CompressorNameKey,
							VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, VideoFormatKeys.DepthKey, 24,
							FormatKeys.FrameRateKey, Rational.valueOf(15), VideoFormatKeys.QualityKey, 1.0f,
							VideoFormatKeys.KeyFrameIntervalKey, 15 * 60),
					new Format(FormatKeys.MediaTypeKey, MediaType.VIDEO, FormatKeys.EncodingKey, "black",
							FormatKeys.FrameRateKey, Rational.valueOf(30)),
					null, new File(dest), tcName);
			this.screenRecorder.start();
		}catch(Exception e){
			log.Report(Priority.ERROR,"Exception : "+e.getMessage());	
		}	
	}*/

	/*public String stopRecording(String tcName)  {
		try{
			String rel = "\\ScreenRecord\\"+tcName+".avi";
			this.screenRecorder.stop();
			return "."+rel;
		}catch(Exception e){
			log.Report(Priority.ERROR,"Exception : "+e.getMessage());
		}
		return "";
	}*/

	
	
}
