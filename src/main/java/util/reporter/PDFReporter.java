package util.reporter;

import java.io.File;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.testng.ITestResult;
import core.framework.Web.Poc.Default;
import core.framework.Web.Poc.Globals;
import lib.Common;
import util.Log;
import util.Log.Priority;
import util.WORD_ReadWrite;
import util.XL_ReadWrite;

public class PDFReporter implements IReporter {

	File files = null;
	WORD_ReadWrite word = null;
	Map<String, String> testExData = null;
	String mtcNo = "";
	int mtcCnt = 1;
	List<String> lstTCNum = new ArrayList<String>();
	List<String> lstOQName = new ArrayList<String>();
	File file = null;
	private static int testItrCount = 0;
	private Log log;

	public PDFReporter(String filePath) {
		files = new File(Globals.GC_TEST_PDFREPORT_DIR);
		if (!files.exists()) {
			files.mkdirs();
		}
		log = new Log(PDFReporter.class);
	}

	public static void SetDataItrCount(int count) {
		testItrCount = count;
	}
	
	@Override
	public void StartTest(String testName) {
		try {
			testExData = new HashMap<String, String>();

			// Get OQ Doc Name and get MTC No from test name
			String OQDocName = getOQDocName(testName);
			mtcNo = testName.substring(testName.length() - 1) + ".1";

			// Prep list of Test name and TC Num to interrupt new 
			lstTCNum.add(testName.substring(testName.lastIndexOf("_") + 1, testName.length()));
			lstOQName.add(testName);

			// To avoid multiple initializing of the file
			if (file == null || (lstOQName.size() > 1
					&& !lstOQName.get(lstOQName.size() - 2).equals(lstOQName.get(lstOQName.size() - 1)))) {
				file = new File(Globals.GC_TEST_PDFREPORT_DIR + "\\" + OQDocName + ".docx");
			}

			// Copy file
			if (!file.exists() & !file.isDirectory()) {
				File fileOQTmp = new File(Globals.GC_OQ_TEMPLATE_PATH + "\\" + OQDocName + ".docx");
				if (fileOQTmp.exists() & !fileOQTmp.isDirectory()) {
					FileUtils.copyFile(new File(Globals.GC_OQ_TEMPLATE_PATH + "\\" + OQDocName + ".docx"),
							new File(Globals.GC_TEST_PDFREPORT_DIR + "\\" + OQDocName + ".docx"));
				} else {
					throw new Exception(OQDocName + ".docx" + " does not exist in OQ_Templates dir");
				}
				mtcCnt = 1;
			} else if (!lstTCNum.get(lstTCNum.size() - 2).equals(lstTCNum.get(lstTCNum.size() - 1))) {
				// resetting test step no decimal to 1
				mtcCnt = 1;
			}

			// Initialize word
			if (file.exists() & !file.isDirectory()) {

				// To avoid multiple initializing of word
				if (lstOQName.size() > 1 && !lstOQName.get(lstOQName.size() - 2).equals(lstOQName.get(lstOQName.size() - 1))) {
					word = new WORD_ReadWrite(Globals.GC_TEST_PDFREPORT_DIR + "\\" + OQDocName + ".docx");
				}else if(lstOQName.size() == 1 ){
					word = new WORD_ReadWrite(Globals.GC_TEST_PDFREPORT_DIR + "\\" + OQDocName + ".docx");
				}

				// Write POST execution details
				writePostTestExecutionDet(word);

				// Update Test Execution Details
				word.ReplaceText("@@URL@", Common.getGlobalParam("URL_" + Common.getGlobalParam("TEST_ENV")), false, 9,"Arial");
				word.ReplaceText("@@BROWSER@",Common.getGlobalParam("BROWSER_NAME") + " -V" + Common.getGlobalParam("BROWSER_VER"), false, 9,"Arial");
				word.ReplaceText("@@COPYDECK@", Common.getGlobalParam("FORMAL_COPYDECK"), false, 9, "Arial");
				word.ReplaceText("@@EVOPROFILE@", Common.getGlobalParam("FORMAL_EVOPROFILE"), false, 9, "Arial");

				// Set test data map
				InitDataMap(testName);

			} else {
				throw new Exception(OQDocName + ".docx" + " does not exist in PDF Report dir");
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
	}
	
	// Read OQ Data Map 
	private void InitDataMap(String testName) {
		try {
			XL_ReadWrite XL = new XL_ReadWrite(Globals.GC_TESTDATALOC + Globals.GC_TESTDATAPREFIX
					+ Common.getGlobalParam("AUT") + "_" + Common.getGlobalParam("TEST_ENV") + Default.getGC_XL_EXTN());

			for (int iRow = 1; iRow <= XL.getRowCount(Globals.GC_OQ_MAP_SHEET); iRow++) {
				if (XL.getCellData(Globals.GC_OQ_MAP_SHEET, iRow, "TestCaseName").trim().equalsIgnoreCase(testName)) {
					List<String> lstOQData = Arrays
							.asList(XL.getCellData(Globals.GC_OQ_MAP_SHEET, iRow, "OQData").trim().split("\\|"));
					for (String data : lstOQData) {
						testExData.put(data.split("@@")[0].trim(), data.split("@@")[1].trim());
					}
					break;
				}
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
	}

	// Update OQ Data Map externally
	public PDFReporter UpdateOQData(String key, String val) {
		try {
			if (testExData != null | !testExData.isEmpty()) {
				if (testExData.containsKey(key)) {
					testExData.replace(key, testExData.get(key), val);
				} else {
					throw new Exception("Key - " + key + " not presentin OQ Data Map");
				}
			} else {
				throw new Exception("OQ Data Map not initialized or its empty");
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
		return this;
	}

	@Override
	public void Log(Status logStatus, String stepName, String stepDetails, boolean captureScreen) {
		try {
			if (!logStatus.equals(Status.INFO)) {

				// Find Target Table and Row
				int tabNo = 0;
				boolean targetRowFound = false;
				List<XWPFTable> tcTab = word.GetAllTable("Test Case/Step", 0);
				XWPFTableRow targetRow = null;
				for (XWPFTable tab : tcTab) {
					for (XWPFTableRow row : tab.getRows()) {
						if (row.getCell(0).getText().equals(mtcNo.split("\\.")[0] + "." + mtcCnt)) {
							targetRow = row;
							targetRowFound = true;
							break;
						}
					}
					if (targetRowFound)
						break;
					tabNo++;
				}

				// Set Column width
				for (int iCell = 0; iCell <= 6; iCell++) {
					targetRow.getCell(iCell).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
				}
				// Set row height
				targetRow.setHeight(5);

				for (int iCell = 1; iCell <= 4; iCell++) {
					targetRow.getCell(iCell).setVerticalAlignment(XWPFVertAlign.TOP);

					XWPFRun rowRun = targetRow.getCell(iCell).addParagraph().createRun();
					rowRun.setFontSize(9);
					rowRun.setFontFamily("Arial");

					switch (iCell) {
					case 1:
						rowRun.setText(stepName);
						break;
					case 2:
						rowRun.setText(stepDetails);
						break;
					case 3:
						rowRun.setText(stepDetails);
						break;
					case 4:
						if (logStatus.equals(Status.FAIL))
							rowRun.setText("N");
						break;
					}
				}

				// Creating new row at the end of entering all col values
				tcTab.get(tabNo).createRow();

				// Entering Step No in newly created row
				targetRow.getCell(0).setVerticalAlignment(XWPFVertAlign.TOP);

				mtcCnt += 1;
				String stepNo = mtcNo.split("\\.")[0] + "." + mtcCnt;

				XWPFRun stepNoRun = tcTab.get(tabNo).getRow(tcTab.get(tabNo).getRows().size() - 1).getCell(0)
						.addParagraph().createRun();
				stepNoRun.setFontSize(9);
				stepNoRun.setFontFamily("Arial");
				stepNoRun.setText(stepNo);
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
	}

	@Override
	public void EndTest(ITestResult result) {
		try {
			if (result.getMethod().getCurrentInvocationCount() == testItrCount) {
				
				// Write Test Execution details at the end of all itr
				String testName = result.getTestContext().getName();
				WriteTestExecutionDet(testName);
							
				// Remove last empty row of the cell
				RemoveEmptyRow();

				// Saving word at the end of all iteration of Test Case
				if (word != null) {
					word.save();
				}
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
	}

	@Override
	public void FlushReport() {
	}

	@Override
	public void CloseReport() {
		try{
			Process p = Runtime.getRuntime().exec("wscript "+Globals.GC_PDF_COVERTER_VBS+"doc2pdf.vbs " + Globals.GC_TEST_PDFREPORT_DIR);
			p.waitFor();
		}catch(Exception e){
			log.Report(Priority.ERROR, e.getMessage());
		}	
	}
	
	private void WriteTestExecutionDet(String testName) {
		try{
			String testNo = testName.substring(testName.lastIndexOf("_") + 1, testName.length()).replace("TC",
					"Test Case ");

			List<XWPFTable> tabs = word.GetAllTable("Test Case Number", 0);

			for (XWPFTable tab : tabs) {
				for (XWPFTableRow row : tab.getRows()) {
					if (row.getCell(0).getText().equals(testNo)) {
						for (Map.Entry<String, String> entry : testExData.entrySet()) {
							word.ReplaceTextInTablePara(row.getCell(2).getParagraphs(), entry.getKey() + ":",
									entry.getKey() + "-" + entry.getValue(), false, 9, "Arial");
						}
						break;
					}
				}
			}
		}catch(Exception e){
			log.Report(Priority.ERROR, e.getMessage());
		}
	}
	
	private void RemoveEmptyRow(){
		try{
			int rowcount = 0;
			boolean targetRowFound = false;
			List<XWPFTable> tcTab = word.GetAllTable("Test Case/Step", 0);
			
			for (XWPFTable tab : tcTab) {
				for (XWPFTableRow row : tab.getRows()) {
					if (row.getCell(0).getText().equals(mtcNo.split("\\.")[0] + "." + mtcCnt)) {
						tab.removeRow(rowcount);
						targetRowFound = true;
						break;
					}
					rowcount++;
				}
				if (targetRowFound) break; rowcount = 0;
			}
		}catch(Exception e){
			log.Report(Priority.ERROR, e.getMessage());
		}
	}

	private void writePostTestExecutionDet(WORD_ReadWrite word) throws Exception {
		XWPFTable tab = word.GetAllTable("Test Performed By", 0).get(0);
		XWPFRun run = null;

		if (tab.getRow(1).getCell(0).getText().equals(Globals.GC_EMPTY)
				& tab.getRow(1).getCell(1).getText().equals(Globals.GC_EMPTY)) {
			DateFormat dfor = new SimpleDateFormat("MM-dd-yyy HH:mm:ss");
			Date dateA = new Date();

			run = tab.getRow(1).getCell(0).addParagraph().createRun();
			run.setBold(true);
			run.setFontFamily("Verdana");
			run.setFontSize(16);
			run.setText(Common.getGlobalParam("FORMAL_AUTHOR"));

			run = tab.getRow(1).getCell(1).addParagraph().createRun();
			run.setBold(false);
			run.setFontFamily("Verdana");
			run.setFontSize(12);
			run.setText(dfor.format(dateA));
		}
	}

	private String getOQDocName(String testName) {
		try {
			XL_ReadWrite XL = new XL_ReadWrite(Globals.GC_TESTDATALOC + Globals.GC_TESTDATAPREFIX
					+ Common.getGlobalParam("AUT") + "_" + Common.getGlobalParam("TEST_ENV") + Default.getGC_XL_EXTN());

			for (int iRow = 1; iRow <= XL.getRowCount(Globals.GC_OQ_MAP_SHEET); iRow++) {
				if (XL.getCellData(Globals.GC_OQ_MAP_SHEET, iRow, "TestCaseName").trim().equalsIgnoreCase(testName)) {
					return XL.getCellData(Globals.GC_OQ_MAP_SHEET, iRow, "OQDocName").trim();
				}
			}
		} catch (Exception e) {
			log.Report(Priority.ERROR, e.getMessage());
		}
		return null;
	}



}
