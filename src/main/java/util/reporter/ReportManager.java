package util.reporter;

import core.framework.Web.Poc.Default;

public class ReportManager {

	private static Reporter report  = new Reporter(Default.getGC_RPT_TYPE());
	
	/**
	 * <h1>GetReporter</h1> used for returning current IReporter object 
	 *  
	 * @return IReporter
	 * @author duttasoc
	 * @date 11/01/2017
	 */
	public synchronized static IReporter GetReporter() {	
		return report;
	}
	
	/**
	 * <h1>GetPDF</h1> used for returning PDF reporter object to access any method only available for PDFReporter class
	 *  
	 * @return IReporter
	 * @author duttasoc
	 * @date 20/03/2017
	 */
	public static IReporter GetPDF(){
		if(report.GetPDFReporter() != null){
		   return report.GetPDFReporter();
		}
		return null;
	}
	
	/*public static void SetOQDataMap(String Key, String Val) {
		if(((PDFReporter) GetPDF()) != null) {
			((PDFReporter) GetPDF()).UpdateOQData(Key, Val);
		}
	}*/
	
	
}
