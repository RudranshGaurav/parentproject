package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import lib.Common;
import util.Log.Priority;


public class WORD_ReadWrite {

	private String path;
	private FileInputStream fin = null;
	public FileOutputStream fout = null;
	private XWPFDocument word;
	private XWPFRun run;
	private Log log = null;
	
	
	public WORD_ReadWrite(String path){
		log =  new Log(WORD_ReadWrite.class);
		this.path = path;
		try{
			fin = new FileInputStream(this.path);
			word = new XWPFDocument(fin);
			fin.close();
		}catch(Exception e){
			log.Report(Priority.ERROR, Common.shortenedStackTrace(e,10));
		}
	}
	
	/**
	 * <h1>CreateParagraph</h1> Create paragraph for a specific styling category
	 * 
	 * @param String setText
	 * @param String  styleCat
	 * @return XWPFParagraph
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public XWPFParagraph CreateParagraph(String setText,String styleCat) throws Exception{
		XWPFParagraph para = null;
		try{
			para = word.createParagraph();
		    run=para.createRun();
		    para.setAlignment(ParagraphAlignment.CENTER);
	    
		    switch(styleCat.toLowerCase().trim()){
		    case "heading" :
		    case "head" : 
		    case "header" : SetHeadingStyle(run); break;    
		    case "body" : SetBodyStyle(run); break;	
		    }
		    run.setText(setText);
		}catch(Exception e){
			throw new Exception("Exception occurred ceating paragraph with text :"+ setText +". Exception details :" +e.getMessage());
		}
		return para;
	}
	
	/**
	 * <h1>SetHeadingStyle</h1> to set heading style for paragraph
	 * 
	 * @param XWPFRun run
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	private void SetHeadingStyle(XWPFRun run){
		run.setBold(true);
		run.setFontSize(15);
		run.setFontFamily("Verdana");
	}
	
	/**
	 * <h1>SetBodyStyle</h1> to set body style for paragraph
	 * 
	 * @param XWPFRun run
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	private void SetBodyStyle(XWPFRun run){
		run.setBold(false);
		run.setFontSize(12);
		run.setFontFamily("Verdana");
	}
	
	/**
	 * <h1>GetAllTable</h1> retrieves list of tables available in the document referencing cell text
	 * 
	 * @param String refColText
	 * @param int refColNum
	 * @return List<XWPFTable>
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public List<XWPFTable> GetAllTable(String refCellText, int refColNum) throws Exception{
		List<XWPFTable> retTables = null;
		try{
			List<XWPFTable> lstTables = word.getTables();
			retTables = new ArrayList<XWPFTable>();
			for (XWPFTable tab : lstTables){
				 if(tab.getRow(0).getCell(refColNum).getText().trim().contains(refCellText.trim())){
					 retTables.add(tab);
				 }
			}
			
		}catch(Exception e){
			throw new Exception("Exception occurred Finding Table : Column Text : "+ refCellText 
							   +". Exception details :" +e.getMessage());
		}
		return retTables;
	}
	

	/**
	 * <h1>GetAllTable</h1> retrieves list of tables available in the document referencing cell text
	 * 
	 * @param String refColText
	 * @param int refRowNum
	 * @param int refColNum
	 * @return List<XWPFTable>
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public List<XWPFTable> GetAllTable(String refCellText, int refRowNum, int refColNum) throws Exception{
		List<XWPFTable> retTables = null;
		try{
			List<XWPFTable> lstTables = word.getTables();
			retTables = new ArrayList<XWPFTable>();
			for (XWPFTable tab : lstTables){
				 if(tab.getRow(refRowNum).getCell(refColNum).getText().trim().contains(refCellText.trim())){
					 retTables.add(tab);
				 }
			}
			
		}catch(Exception e){
			throw new Exception("Exception occurred Finding Table : Column Text : "+ refCellText 
							   +". Exception details :" +e.getMessage());
		}
		return retTables;
	}
	
	/**
	 * <h1>SetTableCellVal</h1> setting table cell value
	 * 
	 * @param XWPFTable tab
	 * @param int rowNum
	 * @param int colNum
	 * @param String text
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public void SetTableCellVal(XWPFTable tab, int rowNum,int colNum,String text) throws Exception{
		try{	
			tab.getRow(rowNum).getCell(colNum).setVerticalAlignment(XWPFVertAlign.CENTER);
			tab.getRow(rowNum).getCell(colNum).setText(text);
		}catch(Exception e){
			throw new Exception("Exception occurred while setting value in cell , row num : "
		                        +rowNum+" and col num : "+colNum+" with set text : "+ text 
		                        +". Exception details :" +e.getMessage());
		}
	}
	
	/**
	 * <h1>save</h1> save the document 
	 * 
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public void save() throws Exception{
		try{
			fout = new FileOutputStream(path);	
			word.write(fout);
			fout.close();
		}catch(Exception e){
			throw new Exception("Exception occurred while saving word. Exception details :" +e.getMessage());
		}	
	}
	

	/**
	 * <h1>UpdateText</h1> finding and update text with certain font style
	 * 
	 * @param String findText
	 * @param  String repText
	 * @param boolean bold
	 * @param int fontSize
	 * @param String fontFamily
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public boolean UpdateText(String findText,String repText,boolean bold,int fontSize,String fontFamily){
		try{
			for (XWPFParagraph p : word.getParagraphs()) {
			    List<XWPFRun> runs = p.getRuns();    
			    if (runs != null && !p.getText().isEmpty()) {
			    	String text = "";
			    	System.out.println("Para - "+p.getText());
			        for (XWPFRun r : runs) {
			            text = r.getText(0);
			            System.out.println("Run - "+r.getText(0));
			            if (text != null && text.contains(findText)) {
			                text = text.replace(findText, repText);
			                r.setBold(bold);
			                r.setFontSize(fontSize);
			                r.setFontFamily(fontFamily);
			                r.setText(text, 0);
			                return true;
			            } 
			        }  
			    }
			}
		}catch(Exception e){
			log.Report(Priority.ERROR, Common.shortenedStackTrace(e,10));
		}
		return false;
	}
	
	/**
	 * <h1>ReplaceText</h1> finding and replace text with certain font style
	 * 
	 * @param String findText
	 * @param  String repText
	 * @param boolean bold
	 * @param int fontSize
	 * @param String fontFamily
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public void ReplaceText(String findText,String repText,boolean bold,int fontSize,String fontFamily){
		try{
			for (XWPFParagraph p : word.getParagraphs()){

		        int numberOfRuns = p.getRuns().size();

		        // Collate text of all runs
		        StringBuilder sb = new StringBuilder();
		        for (XWPFRun r : p.getRuns()){
		            int pos = r.getTextPosition();
		            if(r.getText(pos) != null) {
		                sb.append(r.getText(pos));
		            }
		        }

		        // Continue if there is text and contains findText
		        if(sb.length() > 0 && sb.toString().contains(findText)) {
		            // Remove all existing runs
		            for(int i = numberOfRuns; i >=0 ; i--) {
		                p.removeRun(i);
		            }
		            String text = sb.toString().replace(findText, repText);
		            // Add new run with updated text
		            XWPFRun run = p.createRun();
		            run.setBold(bold);
		            run.setFontSize(fontSize);
		            run.setFontFamily(fontFamily);
		            run.setText(text);
		            p.addRun(run);
		        }
		    } 
		}catch(Exception e){
			log.Report(Priority.ERROR, Common.shortenedStackTrace(e,10));
		}
	}
	
	/**
	 * <h1>ReplaceTextInTablePara</h1> finding and replace text with certain font style in a table paragraph
	 * 
	 * @param List<XWPFParagraph> cellListPara
	 * @param String findText
	 * @param  String repText
	 * @param boolean bold
	 * @param int fontSize
	 * @param String fontFamily
	 * @return void
	 * @author duttasoc
	 * @date 10/02/2017
	 */
	public void ReplaceTextInTablePara(List<XWPFParagraph> cellListPara,String findText,String repText,boolean bold,int fontSize,String fontFamily){
		try{
			for (XWPFParagraph p : cellListPara){

		        int numberOfRuns = p.getRuns().size();

		        // Collate text of all runs
		        StringBuilder sb = new StringBuilder();
		        for (XWPFRun r : p.getRuns()){
		            int pos = r.getTextPosition();
		            if(r.getText(pos) != null) {
		                sb.append(r.getText(pos));
		            }
		        }

		        // Continue if there is text and contains findText
		        if(sb.length() > 0 && sb.toString().contains(findText)) {
		            // Remove all existing runs
		            for(int i = numberOfRuns; i >=0 ; i--) {
		                p.removeRun(i);
		            }
		            String text = sb.toString().replace(findText, repText);
		            // Add new run with updated text
		            XWPFRun run = p.createRun();
		            run.setBold(bold);
		            run.setFontSize(fontSize);
		            run.setFontFamily(fontFamily);
		            run.setText(text);
		            p.addRun(run);
		        }
		    } 
		}catch(Exception e){
			log.Report(Priority.ERROR, Common.shortenedStackTrace(e,10));
		}
	}
	




}
